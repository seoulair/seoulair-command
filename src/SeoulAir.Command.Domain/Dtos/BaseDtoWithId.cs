namespace SeoulAir.Command.Domain.Dtos
{
    public class BaseDtoWithId
    {
        public string Id { get; set; }
    }
}
