namespace SeoulAir.Command.Domain.Options
{
    public class MicroserviceUrlOptions
    {
        public string Address { get; set; }
        public int Port { get; set; }
    }
}
